
$(window).on("load", function () {
    $('.timeline-section .timeline-div .timeline-root .timeline-row .timeline-relative.no-active .timeline-dot').on('click',function(e){
        var round =  $(this).find('.fill-round');
        var fr =  $(this).find('.outline-round2');
        var sr =  $(this).find('.outline-round-root');
        var dot = $(sr).find('.dot');
        var line = $(sr).find('.line');
        var dir = $(this).attr('data-animate');
        var content = $(this).parent().find('.timeline-content');
        var tl = gsap.timeline();
        tl.to(round, {delay: 0, backgroundColor: '#1781E3', width:18,height:18, duration: 0.4, onComplete:function(){
            $(round).addClass('active');
        } });
        tl.to(fr, {  width:34,height:34,  duration: 0.5 });
        tl.to(sr, { width:50,height:50,  duration: 0.5 }); 
        tl.to(dot, { opacity:1,  duration: 0.5 }); 
        tl.to(line, {  height:69,  duration: 0.5 }); 
        if(dir == 'top'){
            tl.to(content, {   opacity:1, y:-50,   duration: 0.5 }); 
        }else{
            tl.to(content, {   opacity:1, y:50,   duration: 0.5 }); 
        }
        
    });
});