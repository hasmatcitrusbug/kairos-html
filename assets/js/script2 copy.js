

//scroll.addMomentum(0, 100);


// var myFullpage = new fullpage('#slide_section', {
//     anchors: ['slide_1', 'slide_2', 'slide_3'],
//     scrollBar: true
// });




$(window).on("load", function () {

    var scroll = Scrollbar.init(
        document.querySelector("#intro-section"), {
        damping: 0.1,
        thumbMinSize: 0,
        renderByPixels: false
    });

    var device = '';
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        device = 'mobile';
    } else {
        device = 'desktop';
    }

    console.log(device);

    function activeMenu(i) {
        $(".slide_nav li").removeClass('active');
        $(".slide_nav li:nth-child(" + i + ")").addClass('active');
    }

    if (device == 'desktop') {
        if (device == 'desktop') {
        $('.video_play').on('click', function () {
            $("#main_video").trigger('play');
            $(this).hide();
        });

        $('.skip_video').on('click', function () {
            // $("#slide_section").show();
            scroll.update()


            // $("#video_section").hide();
            var tl = gsap.timeline();
            tl.to('.slide_nav', 1, { top: '40%', opacity: 1, duration: 1 });
            tl.to('.intro-section.slide_section .slides#slide_1 h1', { delay: 0, duration: 1, bottom: '40%', opacity: 1, });
            tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 })

            scroll.scrollIntoView(document.querySelector("#slide_section"));

            activeMenu(1)
        });
        $('#main_video').on('ended', function () {
            // $("#slide_section").show();
            scroll.update()


            // $("#video_section").hide();
            var tl = gsap.timeline();
            tl.to('.slide_nav', 1, { top: '40%', opacity: 1, duration: 1 });
            tl.to('.intro-section.slide_section .slides#slide_1 h1', { delay: 0, duration: 1, bottom: '40%', opacity: 1, });
            tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 })

            scroll.scrollIntoView(document.querySelector("#slide_section"));

            activeMenu(1)
        });


        $("#video_section").bind('mousewheel', function (e) {

            e.preventDefault();
            if (e.originalEvent.wheelDelta < 0) {

                var tl = gsap.timeline();
                tl.to('.slide_nav', 1, { top: '40%', opacity: 1, duration: 1 });
                tl.to('.intro-section.slide_section .slides#slide_1 h1', { delay: 0, duration: 1, bottom: '40%', opacity: 1, });
                tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 })


                scroll.scrollIntoView(document.querySelector("#slide_section", { onlyScrollIfNeeded: true, }));

                activeMenu(1)

            } else {
                scroll.scrollIntoView(document.querySelector("#video_section", { onlyScrollIfNeeded: true, }));

            }
            return false;
        });

        $("#slide_1").bind('mousewheel', function (e) {

            e.preventDefault();
            if (e.originalEvent.wheelDelta < 0) {

                var tl = gsap.timeline();
                tl.to('.intro-section.slide_section .slides#slide_1 h1', { duration: 1, bottom: 0, opacity: 0, });
                tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });


                var t2 = gsap.timeline();
                t2.to('.intro-section.slide_section .slides#slide_2 h1', { delay: 0.5, duration: 1, bottom: '40%', opacity: 1, });
                t2.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 });

                scroll.scrollIntoView(document.querySelector("#slide_2", { onlyScrollIfNeeded: true, }));

                activeMenu(2)

            } else {

                var tl = gsap.timeline();
                tl.to('.slide_nav', 1, { top: '0', opacity: 0, duration: 1 });
                tl.to('.intro-section.slide_section .slides#slide_1 h1', { duration: 1, bottom: 0, opacity: 0, });
                tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });

                scroll.scrollIntoView(document.querySelector("#video_section", { onlyScrollIfNeeded: true, }));
            }
            return false;
        });

        $("#slide_2").bind('mousewheel', function (e) {

            e.preventDefault();
            if (e.originalEvent.wheelDelta < 0) {

                var tl = gsap.timeline();
                tl.to('.intro-section.slide_section .slides#slide_2 h1', { duration: 1, bottom: 0, opacity: 0, });
                tl.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });

                var t2 = gsap.timeline();
                t2.to('.intro-section.slide_section .slides#slide_3 h1', { delay: 0.5, duration: 1, bottom: '40%', opacity: 1, });
                t2.to('.intro-section.slide_section .slides#slide_3 .scroll_up', { duration: 1, top: '0%', opacity: 1 });

                scroll.scrollIntoView(document.querySelector("#slide_3", { onlyScrollIfNeeded: true, }));

                activeMenu(3)

            } else {

                var tl = gsap.timeline();

                tl.to('.intro-section.slide_section .slides#slide_2 h1', { duration: 1, bottom: 0, opacity: 0, });
                tl.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });

                tl.to('.intro-section.slide_section .slides#slide_1 h1', { delay: 0, duration: 1, bottom: '40%', opacity: 1, });
                tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 })

                scroll.scrollIntoView(document.querySelector("#slide_1", { onlyScrollIfNeeded: true, }));

                activeMenu(1)

            }
            return false;
        });

        $("#slide_3").bind('mousewheel', function (e) {

            e.preventDefault();
            if (e.originalEvent.wheelDelta < 0) {
                scroll.scrollIntoView(document.querySelector("#slide_3", { onlyScrollIfNeeded: true, }));
            } else {

                var tl = gsap.timeline();

                tl.to('.intro-section.slide_section .slides#slide_3 h1', { duration: 1, bottom: 0, opacity: 0, });
                tl.to('.intro-section.slide_section .slides#slide_3 .scroll_up', { duration: 1, top: -300, opacity: 0 });

                tl.to('.intro-section.slide_section .slides#slide_2 h1', { delay: 0, duration: 1, bottom: '40%', opacity: 1, });
                tl.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 })

                scroll.scrollIntoView(document.querySelector("#slide_2", { onlyScrollIfNeeded: true, }));

                activeMenu(2)

            }
            return false;
        });


        $('.slide_nav li').on('click', function () {
            // $(".slide_nav li").removeClass('active');
            // $(this).addClass('active');
            // console.log($('.slide_nav li.active a').attr('href'));
            // scroll.scrollIntoView(document.querySelector($('.slide_nav li.active a').attr('href')));

        });

    } else {


        var controller = new ScrollMagic.Controller();
        var slide_nav = new TimelineMax();
        slide_nav.to('.slide_nav', 1, { top: '40%', opacity: 1, duration: 1 })


        var timelinescene = new TimelineMax();

        timelinescene.add(slide_nav);

        new ScrollMagic.Scene({
            triggerElement: '.intro-section.slide_section .slides#slide_1',
            triggerHook: 0.2,
            duration: "28%"
        })
            .setTween(timelinescene)
            // .addIndicators({
            //     colorTrigger: "black",
            //     colorStart: "white",
            //     colorEnd: "white",
            //     indent: 30
            //   })
            .addTo(controller);


      
        function setupAnimation(i) {



            var timelinescene = new TimelineMax();
            var tlh1 = new TimelineMax();
            tlh1.to('.intro-section.slide_section .slides#slide_' + i + ' h1', 1, { delay: 0, bottom: '40%', opacity: 1, })
                .to('.intro-section.slide_section .slides#slide_' + i + ' h1', 1, { delay: 0, bottom: '100%', opacity: 0, });
            timelinescene.add(tlh1);
            timelinescene.add(scroll_down);
            var Scene = new ScrollMagic.Scene({
                triggerElement: '.intro-section.slide_section .slides#slide_' + i + '',
                triggerHook: 0.2,
                duration: "55%"
            })
                .setTween(timelinescene)
                // .setTween(t2)
                // .addIndicators({
                //     colorTrigger: "black",
                //     colorStart: "white",
                //     colorEnd: "white",
                //     indent: 10
                //   })
                .on("enter", function (event) {
                    activeMenu(i)

                })
                .addTo(controller);



            var scroll_down = new TimelineMax();
            scroll_down.staggerTo('.intro-section.slide_section .slides#slide_' + i + ' .scroll_down', 1, { delay: 2, bottom: '0%', opacity: 1 })
                .to('.intro-section.slide_section .slides#slide_' + i + ' .scroll_down', 1, { delay: 2, bottom: '100%', opacity: 0 });
            new ScrollMagic.Scene({
                triggerElement: '.intro-section.slide_section .slides#slide_' + i + '',
                triggerHook: 0.3,
                duration: "60%"
            })
                .setTween(scroll_down)
                // .setTween(t2)
                // .addIndicators({
                //     colorTrigger: "black",
                //     colorStart: "white",
                //     colorEnd: "white",
                //     indent: 20
                //   })
                .addTo(controller);





        }

        setupAnimation(1);
        setupAnimation(2);


        var timelinescene = new TimelineMax();
        var tlh1 = new TimelineMax();
        tlh1.to('.intro-section.slide_section .slides#slide_3 h1', 1, { delay: 0, bottom: '40%', opacity: 1, })
            .to('.intro-section.slide_section .slides#slide_3 h1', 1, { delay: 0, bottom: '100%', opacity: 0, });
        timelinescene.add(tlh1);
        timelinescene.add(scroll_down);
        new ScrollMagic.Scene({
            triggerElement: '.intro-section.slide_section .slides#slide_3',
            triggerHook: 0.2,
            duration: "55%"
        })
            .setTween(timelinescene)
            // .setTween(t2)
            // .addIndicators({
            //     colorTrigger: "black",
            //     colorStart: "white",
            //     colorEnd: "white",
            //     indent: 10
            //   })
            .addTo(controller);


        var scroll_down = new TimelineMax();
        scroll_down.staggerTo('.intro-section.slide_section .slides#slide_3 .scroll_up', 1, { delay: 2, top: '-48', opacity: 1 })
            .to('.intro-section.slide_section .slides#slide_3 .scroll_up', 1, { delay: 2, top: '100%', opacity: 0 });
        var Scene = new ScrollMagic.Scene({
            triggerElement: '.intro-section.slide_section .slides#slide_3',
            triggerHook: 0.3,
            duration: "60%"
        })
            .setTween(scroll_down)
            // .setTween(t2)
            // .addIndicators({
            //     colorTrigger: "black",
            //     colorStart: "white",
            //     colorEnd: "white",
            //     indent: 20
            //   })
            .on("enter", function (event) {
                activeMenu(3)

            })
            .addTo(controller);


        $('.slide_nav li').on('click', function () {
            $(".slide_nav li").removeClass('active');
            $(this).addClass('active');
            console.log($('.slide_nav li.active a').attr('href'));
            scroll.scrollIntoView(document.querySelector($('.slide_nav li.active a').attr('href')));

        });

    }




 


    // $("#slide_1").bind('swipeDown', function (e) {

    //     e.preventDefault();

    //     var tl = gsap.timeline();
    //     tl.to('.intro-section.slide_section .slides#slide_1 h1', { duration: 1, bottom: 0, opacity: 0, });
    //     tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });


    //     var t2 = gsap.timeline();
    //     t2.to('.intro-section.slide_section .slides#slide_2 h1', { delay:0.5, duration: 1, bottom: '40%', opacity: 1, });
    //     t2.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 });

    //     scroll.scrollIntoView(document.querySelector("#slide_2", { onlyScrollIfNeeded: true, }));

    //     activeMenu(2);
    //     return false;
    // });
    // $("#slide_1").bind('swipeUp', function (e) {
    //     e.preventDefault();
    //     var tl = gsap.timeline();
    //     tl.to('.slide_nav', 1, { top: '0', opacity: 0, duration: 1 });
    //     tl.to('.intro-section.slide_section .slides#slide_1 h1', { duration: 1, bottom: 0, opacity: 0, });
    //     tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });
    //     scroll.scrollIntoView(document.querySelector("#video_section", { onlyScrollIfNeeded: true, }));
    //     return false;
    // });



    // $("#slide_2").bind('swipeDown', function (e) {
    //     e.preventDefault();
    //     var tl = gsap.timeline();
    //         tl.to('.intro-section.slide_section .slides#slide_2 h1', { duration: 1, bottom: 0, opacity: 0, });
    //         tl.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });

    //         var t2 = gsap.timeline();
    //         t2.to('.intro-section.slide_section .slides#slide_3 h1', { delay:0.5, duration: 1, bottom: '40%', opacity: 1, });
    //         t2.to('.intro-section.slide_section .slides#slide_3 .scroll_up', { duration: 1, top: '0%', opacity: 1 });

    //         scroll.scrollIntoView(document.querySelector("#slide_3", { onlyScrollIfNeeded: true, }));

    //         activeMenu(3)
    //     return false;
    // });
    // $("#slide_2").bind('swipeUp', function (e) {
    //     e.preventDefault();
    //     var tl = gsap.timeline();

    //         tl.to('.intro-section.slide_section .slides#slide_2 h1', { duration: 1, bottom: 0, opacity: 0, });
    //         tl.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: -300, opacity: 0 });

    //         tl.to('.intro-section.slide_section .slides#slide_1 h1', { delay: 0, duration: 1, bottom: '40%', opacity: 1, });
    //         tl.to('.intro-section.slide_section .slides#slide_1 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 })

    //         scroll.scrollIntoView(document.querySelector("#slide_1", { onlyScrollIfNeeded: true, }));

    //         activeMenu(1)
    //     return false;
    // });



    // $("#slide_3").bind('swipeDown', function (e) {
    //     e.preventDefault();
    //     return false;
    // });
    // $("#slide_3").bind('swipeUp', function (e) {
    //     e.preventDefault();
    //     var tl = gsap.timeline();

    //     tl.to('.intro-section.slide_section .slides#slide_3 h1', { duration: 1, bottom: 0, opacity: 0, });
    //     tl.to('.intro-section.slide_section .slides#slide_3 .scroll_up', { duration: 1, top: -300, opacity: 0 });

    //     tl.to('.intro-section.slide_section .slides#slide_2 h1', { delay: 0, duration: 1, bottom: '40%', opacity: 1, });
    //     tl.to('.intro-section.slide_section .slides#slide_2 .scroll_down', { duration: 1, bottom: '0%', opacity: 1 })

    //     scroll.scrollIntoView(document.querySelector("#slide_2", { onlyScrollIfNeeded: true, }));

    //     activeMenu(2)
    //     return false;
    // });




});

