
$('#banner-owl').owlCarousel({
    loop:true,
    nav:false,
    navText: ['<span class="span-roundcircle left-roundcircle"><i class="icon icon-014-chevron-left"></i></span>','<span class="span-roundcircle right-roundcircle"><i class="icon icon-015-chevron-right"></i></span>'],
    dots: true,
    stagePadding: 0,
    autoplay:true,
    smartSpeed:2000,
    responsive:{
        0:{
            items:1 
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  });
  

$('#carousel-custom1').owlCarousel({
    loop:false,
    nav:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><i class="icon icon-014-chevron-left"></i></span>','<span class="span-roundcircle right-roundcircle"><i class="icon icon-015-chevron-right"></i></span>'],
    dots: true,
    stagePadding: 0,
    autoplay:true,
    smartSpeed:2000,
    responsive:{
        0:{
            items:1 
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
  });

  