$( document ).ready(function() {
    $('.video_play').on('click',function(){
        $("#main_video").trigger('play'); 
        $(this).hide();
    });

    $('.skip_video').on('click',function(){
        animateToslides();
    });

    $('#main_video').on('ended',function(){
        animateToslides();
    });


     //IE, Opera, Safari
    $('#slide_1').bind('mousewheel', function(e){
        e.preventDefault();
        if(e.originalEvent.wheelDelta < 0) {
            animateSlide(1,2,true);
        }else {
            animateVideo()
        } 
        return false;
    });


    $('#slide_2').bind('mousewheel', function(e){
        e.preventDefault();
        if(e.originalEvent.wheelDelta < 0) {
            animateSlide(2,3,true);
        }else {
            animateSlide(2,1,false);
        } 
        return false;
    });


    $('#slide_3').bind('mousewheel', function(e){
        e.preventDefault();
        if(e.originalEvent.wheelDelta < 0) {
            
        }else { 
            animateSlide(3,2,false);
        }  
        return false;
    });

});

function activeMenu(i){
    $(".slide_nav li").removeClass('active');
    $(".slide_nav li:nth-child("+i+")").addClass('active');
} 

function animateSlide(from, to, forward){
    if(forward){
        gsap.to("#slide_"+from+" h1" , { bottom:'80%', opacity: 0,  duration: 3, delay:0 });

        gsap.to("#slide_"+from+" .scroll_down" , { bottom:'200px', opacity: 0,  duration: 2, delay:0 });

        gsap.to("#slide_"+from+"" , { display:'none',  duration: 0, delay:2.5 });
        

        gsap.to("#slide_"+to+" h1" , { bottom:'40%' , opacity: 1,  duration: 3, delay:2 });

        gsap.to("#slide_"+to+" .scroll_down" , { bottom:'0px', opacity: 1,  duration: 2, delay:3 });

        gsap.to("#slide_"+to+" .scroll_up" , { top:'0px', opacity: 1,  duration: 2, delay:3 });
 
    }else{

        gsap.to("#slide_"+from+" .scroll_up" , { top:'-300px', opacity: 0,  duration: 2, delay:0 });

        gsap.to("#slide_"+from+" .scroll_down" , { bottom:'-300px', opacity: 0,  duration: 2, delay:0 });

        gsap.to("#slide_"+from+" h1" , { bottom:'0', opacity: 0,  duration: 3, delay:0 });
    
    
        gsap.to("#slide_"+to+"" , { display:'flex',  duration: 0, delay:2.5 });
    
        gsap.to("#slide_"+to+" h1" , { bottom:'40%', opacity: 1,  duration: 3, delay:2 });

        gsap.to("#slide_"+to+" .scroll_down" , { bottom:'0px', opacity: 1,  duration: 2, delay:3 });
    
        
    }
    activeMenu(to);
}
 
function animateToslides(){
 
    gsap.to(".slide_section" , {display:'block', duration: 0, delay:0 });

    gsap.to(".video_section" , {opacity: 0,  duration: 1, delay:0 });

    gsap.to(".slide_section" , {display:'block', opacity: 1,  y:'-100%',  duration: 2, delay:1 });
    

    gsap.to(".slide_nav" , { top:'40%',  duration: 3, delay:2.7 });

    gsap.to("#slide_1 h1" , { bottom:'40%', opacity: 1,   duration: 3, delay:2.7 });

    gsap.to("#slide_1 .scroll_down" , { bottom:'0px', opacity: 1,  duration: 3, delay:5 });
    
}

function animateVideo(){


    gsap.to("#slide_1 .scroll_down" , { bottom:'-300px', opacity: 0,  duration: 3, delay:0 });

    gsap.to("#slide_1 h1" , { bottom:'0', opacity: 0,  duration: 3, delay:1 });
 
    gsap.to(".slide_nav" , { top:'0',  duration: 3, delay:1.5 });

    gsap.to(".slide_section" , { display:'none',  y:'100%',  duration: 0, delay:2.5 });
    
    gsap.to(".video_section" , {opacity: 1,  duration: 1, delay:2.5 });
  
}